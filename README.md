# fraudTestFG

projekt iz predmeta ASP

## Getting started

Za projektni zadatak su mi bile potrebne ove biblioteke: iostream, fstream, string, queue, chrono, vector i algorithm. Za početak sam trebao napraviti klasu, kod mene se zove Fraud. U njoj je postavljen defaultni konstruktor koji prvo izvlači podatke iz datoteke fraudTest.csv, nakon toga sam stvorio tri varijable u kojima ću spremati podatke. Nakon toga sam provjeravao je li datoteka fraudTest.csv stvorena, ako nije vraća da nije i suprotno. Kada datoteka je otvorena, prva stvar koju sam napravio je stvorio varijablu tipa string line u kojoj se sprema fstream iz datoteke. Poslije toga mi treba getline kako bi preskočio prvi red jer prvi red su imena stupaca. Nakon toga ide while petlja s getline uvjetom. U njoj za prvi stupac imam varijablu num u kojoj se pretvara string u integer (stoi), gdje počinje s pozicijom 0 do prvo zareza. Podatkovni članovi klase Fraud su prioritetni redovi i vektori. Koristim prioritetne redove kako bih dobio najveći i najmanji broj iz datoteke fraudTest.csv, a vektore za brisanje, dodavanje i pretragu. Nakon toga se inicijaliziraju varijable counter i brojac koje s dvije for petlje (svaka varijabla ima zasebne dvije for petlje) unesemo vrijednosti u prioritetne redove i vektore. S vanjskim for petljama tražim stupac koji ću koristiti, a unutarnjim for petljama tražim završni zarez tj. gdje taj stupac završava. U varijable t i zip koje pretvaraju string u double se spremaju podatci sa stupaca 13 i 14. Svaki stupac ima svoju funkciju. 
Mjerenje vremena se postiže korištenjem bibliotekom chrono. Vrijeme potrebno da se cijeli defaultni konstruktor dovrši je od 5 do 9 sekundi tj. od 5000ms do 9000ms. Razlog tome je što je A priori najgoreg slučaja najvjerojatnije O(n^2). 

Za najveće i najmanje vrijednosti kao što rekoh koristim prioritetni red, za najmanje vrijednosti se mora koristiti compare kako bih napravio min heap dok za najveće prioritetni red već stvara max heap. Vrijeme izvršavanja u kojem vraća samo najveću ili najmanju vrijednost iznosi oko 1ms. Za n najvećih i najmanjih vrijednosti vrijeme iznosi isto od 1-2ms. Razlog tako brzom izvršavanju je što gomila ima konstantno vrijeme pokazivanja najveće ili najmanje vrijednosti. 

Dodavanje vrijednosti se izvodi tako da napisemo imeVektora.push_back(vrijednost); Za dodavanje n vrijednosti koristim vektore koje stvorim u main funkciji i proslijedim funkcijama. Napravio sam function overloading. Vrijeme izvršavanja je oko 1ms jer dodavanje na kraj vektora je O(1).

Brisanje vrijednosti se izvodi tako da imamo binary search funckiju koja nam pretražuje vrijednost u vektoru. Ako ju pronađe, izbriše ju, ako ne ispiše da vrijednost ne postoji. Za prvi stupac vrijeme izvršavanja je oko 9ms za jednu vrijednost, dok za njih više oko 5ms. Za stupce 13 i 14 vrijeme izvršavnja je oko 200ms jer se prije binary search funkcije sortiraju vektori. I identično je vrijeme kada proslijedim vektor vrijednosti za brisanje n vrijednosti. 

Pretraživanje vrijednosti se izvodi na identičan način kao brisanje samo što umjesto da obriše vrijednosti ispiše je li ju je pronašao ili nije. Za stupce 13 i 14 se prvo sortiraju vektori u kojima su spremljene vrijednosti radi lakše pretrage. Vrijeme izvršavanja za prvi stupac za jednu vrijednost je oko 1ms te za n vrijednosti je isto 1ms. Za stupce 13 i 14 pretraživanje jedne vrijednosti traje oko 200ms bez obzira je li na kraju ili na početku. Za pretragu n vrijednosti u stupcima 13 i 14 je isto vrijeme izvršavanja oko 200ms. 

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <vector>
#include <chrono>
#include <algorithm>


class Fraud{
public:
    Fraud() {
        std::ifstream datoteka("C:/Users/Public/clion/fraudTest/fraudTest.csv");
        int num;
        double zip;
        double t;
        if (datoteka.is_open()) {
            if (!datoteka) {
                std::cout << "File not created!" << std::endl;
            } else {
                std::cout << "File created successfully!" << std::endl;

                if (datoteka.good()) {
                    std::string line;
                    getline(datoteka, line);
                    while (getline(datoteka, line)) {
                        num=std::stoi(line.substr(0, line.find(",")));
                        queue1.push(num);
                        queue2.push(num);
                        v.push_back(num);
                        int counter=0;
                        int brojac=0;
                        for (int i = 0; i < line.length(); i++) {
                            if (line[i] == ',') {
                                counter++;
                            }
                            if (line[i] == ',' && counter == 14) {
                                int j;
                                for (j = i + 1; j < line.length(); j++) {
                                    if (line[j] == ',') {
                                        break;
                                    }
                                }
                                zip=std::stod( line.substr(i+1,j-i-1 ));
                                zipV.push_back(zip);
                                queue3.push(zip);
                                queue4.push(zip);
                            }
                        }
                        for (int i = 0; i < line.length(); i++) {
                            if (line[i] == ',') {
                                brojac++;
                            }
                            if (line[i] == ',' && brojac == 13) {
                                int j;
                                for (j = i + 1; j < line.length(); j++) {
                                    if (line[j] == ',') {
                                        break;
                                    }
                                }
                                t=std::stod( line.substr(i+1,j-i-1 ));
                                tV.push_back(t);
                                queue5.push(t);
                                queue6.push(t);
                            }
                        }
                    }
                    datoteka.close();
                }
            }
        }
    }

    int get_top_value() {
        return queue1.top();
    }

    void get_top_value(int n){
        std::vector<int> v;
        for(int i=0; i<n;i++){
            v.push_back(queue1.top());
            queue1.pop();
        }
        for(int i=0;i<n;i++){
            queue1.push(v[i]);
        }
       for(auto it:v){
           std::cout<<it<<" ";
       }
    }

    int get_bottom_value() {
        return queue2.top();
    }

    void get_bottom_value(int n){
        std::vector<int> v;
        for(int i=0; i<n;i++){
            v.push_back(queue2.top());
            queue2.pop();
        }
        for(int i=0;i<n;i++){
            queue2.push(v[i]);
        }
        for(auto it:v){
            std::cout<<it<<" ";
        }
    }

    long double get_top_value2() {
        return queue3.top();
    }

    void get_top_value2(int n){
        std::vector<long double> v;
        for(int i=0; i<n;i++){
            v.push_back(queue3.top());
            queue3.pop();
        }
        for(int i=0;i<n;i++){
            queue3.push(v[i]);
        }
        for(auto it:v){
            std::cout<<it<<" ";
        }
    }

    long double get_top_value3() {
        return queue5.top();
    }

    void get_top_value3(int n){
        std::vector<long double> v;
        for(int i=0; i<n;i++){
            v.push_back(queue5.top());
            queue5.pop();
        }
        for(int i=0;i<n;i++){
            queue5.push(v[i]);
        }
        for(auto it:v){
            std::cout<<it<<" ";
        }
    }

    long double get_bottom_value2() {
        return queue4.top();
    }

    void get_bottom_value2(int n){
        std::vector<long double> v;
        for(int i=0; i<n;i++){
            v.push_back(queue4.top());
            queue4.pop();
        }
        for(int i=0;i<n;i++){
            queue4.push(v[i]);
        }
        for(auto it:v){
            std::cout<<it<<" ";
        }
    }

    long double get_bottom_value3() {
        return queue6.top();
    }

    void get_bottom_value3(int n){
        std::vector<long double> v;
        for(int i=0; i<n;i++){
            v.push_back(queue6.top());
            queue6.pop();
        }
        for(int i=0;i<n;i++){
            queue6.push(v[i]);
        }
        for(auto it:v){
            std::cout<<it<<" ";
        }
    }

    void addValue(int n ){
        v.push_back(n);
    }

    void addValue2(double n ){
        zipV.push_back(n);
    }

    void addValue3(double n ){
        tV.push_back(n);
    }

    void addValue(std::vector<int> &b){
        for(int i=0;i<b.size();i++){
            v.push_back(b[i]);
        }
    }

    void addValue2(std::vector<double> &b){
        for(int i=0;i<b.size();i++){
            zipV.push_back(b[i]);
        }
    }

    void addValue3(std::vector<double> &b){
        for(int i=0;i<b.size();i++){
            tV.push_back(b[i]);
        }
    }

    void deleteValue(int x){
        if(std::binary_search(v.begin(),v.end(),x)){
            v.erase(std::remove(v.begin(),v.end(),x),v.end());
        }
        else{
            std::cout<<"Value does not exist"<<std::endl;
        }
    }

    void deleteValue2(double x){
        std::sort(zipV.begin(),zipV.end());
        if(std::binary_search(zipV.begin(),zipV.end(),x)){
            zipV.erase(std::remove(zipV.begin(),zipV.end(),x),zipV.end());
        }
        else{
            std::cout<<"Value does not exist"<<std::endl;
        }
    }

    void deleteValue3(double x){
        std::sort(tV.begin(),tV.end());
        if(std::binary_search(tV.begin(),tV.end(),x)){
            tV.erase(std::remove(tV.begin(),tV.end(),x),tV.end());
        }
        else{
            std::cout<<"Value does not exist"<<std::endl;
        }
    }

    void deleteValue(std::vector<int> &a){
        for(int i=0;i<a.size();i++){
            if(std::binary_search(v.begin(),v.end(),a[i])){
                v.erase(std::remove(v.begin(),v.end(),a[i]),v.end());
            }
            else{
                std::cout<<"Values do not exist"<<std::endl;
            }
        }
    }

    void deleteValue2(std::vector<double> &a){
        std::sort(zipV.begin(),zipV.end());
        for(int i=0;i<a.size();i++){
            if(std::binary_search(zipV.begin(),zipV.end(),a[i])){
                zipV.erase(std::remove(zipV.begin(),zipV.end(),a[i]),zipV.end());
            }
            else{
                std::cout<<"Values do not exist"<<std::endl;
            }
        }
    }

    void deleteValue3(std::vector<double> &a){
        std::sort(tV.begin(),tV.end());
        for(int i=0;i<a.size();i++){
            if(std::binary_search(tV.begin(),tV.end(),a[i])){
                tV.erase(std::remove(tV.begin(),tV.end(),a[i]),tV.end());
            }
            else{
                std::cout<<"Values do not exist"<<std::endl;
            }
        }
    }

    void search(int x){
            if(std::binary_search(v.begin(),v.end(),x)){
                std::cout<<"Found: "<<x<<std::endl;
            }
            else{
                std::cout<<"Didn't found: "<<x<<std::endl;
            }
    }

    void search2(double x){
        std::sort(zipV.begin(),zipV.end());
        if(std::binary_search(zipV.begin(),zipV.end(),x)){
            std::cout<<"Found: "<<x<<std::endl;
        }
        else{
            std::cout<<"Didn't found: "<<x<<std::endl;
        }
    }

    void search3(double x){
        std::sort(tV.begin(),tV.end());
        if(std::binary_search(tV.begin(),tV.end(),x)){
            std::cout<<"Found: "<<x<<std::endl;
        }
        else{
            std::cout<<"Didn't found: "<<x<<std::endl;
        }


    }

    void search(std::vector<int> &d){
        for(int i=0;i<d.size();i++){
            if(std::binary_search(v.begin(),v.end(),d[i])){
                std::cout<<"Found value: "<<d[i]<<std::endl;
            }
            else{
                std::cout<<"Didn't found value: "<<d[i]<<std::endl;
            }
        }
    }

    void search2(std::vector<double> &d){
        std::sort(zipV.begin(),zipV.end());
        for(int i=0;i<d.size();i++){
            if(std::binary_search(zipV.begin(),zipV.end(),d[i])){
                std::cout<<"Found value: "<<d[i]<<std::endl;
            }
            else{
                std::cout<<"Didn't found value: "<<d[i]<<std::endl;
            }
        }
    }

    void search3(std::vector<double> &d){
        std::sort(tV.begin(),tV.end());
        for(int i=0;i<d.size();i++){
            if(std::binary_search(tV.begin(),tV.end(),d[i])){
                std::cout<<"Found value: "<<d[i]<<std::endl;
            }
            else{
                std::cout<<"Didn't found value: "<<d[i]<<std::endl;
            }
        }
    }

    void print(){
       std::cout<<v.back()<<std::endl;
    }

private:
    std::priority_queue<int> queue1;
    std::priority_queue<int, std::vector<int>,std::greater<int>> queue2;
    std::vector<int> v;
    std::vector<long double> zipV;
    std::vector<long double> tV;
    std::priority_queue<long double> queue3;
    std::priority_queue<long double, std::vector<long double>,std::greater<long double>> queue4;
    std::priority_queue<long double> queue5;
    std::priority_queue<long double, std::vector<long double>,std::greater<long double>> queue6;
};
int main(){
    auto start = std::chrono::high_resolution_clock::now();
    Fraud f;
    std::cout<<std::endl;
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration =std::chrono::duration_cast<std::chrono::milliseconds>(stop -start);
    std::cout << "Vrijeme: " <<duration.count() << "ms" << std::endl;

    //vrijeme izvrsenja je 7769ms

    std::cout<<std::endl;

    auto start1 = std::chrono::high_resolution_clock::now();

    //najvece i najmanje vrijednosti:

    //std::cout<<f.get_top_value(); vrijeme izvrsenja 1ms
    //std::cout<<f.get_bottom_value(); vrijeme izvrsenja 0ms
    //std::cout<<f.get_top_value2(); //vrijeme izvrsenja 0ms
   // std::cout<<f.get_bottom_value2(); //vrijeme izvrsenja 1ms
    //std::cout<<f.get_top_value3()<<std::endl; //vrijeme izvršenja 0ms
    // std::cout<<f.get_bottom_value3(); //vrijeme izvršenja 0ms

    //f.get_top_value(10);// vrijeme izvrsenja 1ms
   // f.get_bottom_value(10); //vrijeme izvrsenja 1ms
   //f.get_top_value2(10); //vrijeme izvrsenja 2ms
    //f.get_bottom_value2(10); //vrijeme izvrsenja 1ms
    //f.get_top_value3(10); //vrijeme izvrsenja 1ms
   // f.get_bottom_value3(10); //vrijeme izvrsenja 1ms
    std::cout<<std::endl;

    //dodavanje vrijednosti:
    //f.addValue(555720); vrijeme izvrsavanja 0ms
    //f.addValue2(78.47); vrijeme izvrsavanja 0ms
    //f.addValue3(420.00001); vrijeme izvrsavanja 1ms
    //std::vector<int> v1={555721,555722};
   // std::vector<double> v2={45.123,999,0.0001};
    //f.addValue(v1); vrijeme izvrsavanja 0ms
    // f.addValue2(v2); vrijeme izvrsavanja 0ms
    //f.addValue3(v2); vrijeme izvrsavanja 1ms

    //brisanje vrijednosti:
    //f.deleteValue(1); //vrijeme izvrsavanja  9ms
    //f.deleteValue(555719); //vrijeme izvrsavanja 9ms
    //f.deleteValue2(65.6899); //vrijeme izvrsavanja 144ms
    //f.deleteValue2(-165.572); //vrijeme izvrsavanja 138ms
    //f.deleteValue3(20.0271); //vrijeme izvrsavanja 201ms
    //f.deleteValue3(99921); //vrijeme izvrsavanja 201ms
    std::vector<int> v1={555718,555700};
     std::vector<double> v2={45.123,99921,0.0001};
     //f.deleteValue(v1); vrijeme izvrsavanja 5ms
     //f.deleteValue2(v2); vrijeme izvrsavanja 202ms
     //f.deleteValue3(v2); vrijeme izvrsavanja 201ms

    //pretrazivanje vrijednosti:
    //f.search(1); //vrijeme izvrsavanja  0ms
    //f.search(555718); //vrijeme izvrsavanja 0ms
   // f.search2(65.6899); //vrijeme izvrsavanja 203ms
    //f.search2(-165.572); //vrijeme izvrsavanja 198ms
    //f.search3(20.0271); //vrijeme izvrsavanja 194ms
    //f.search3(99921); //vrijeme izvrsavanja 196ms
    std::vector<int> v3={555718,555700};
    std::vector<double> v4={65.6899,99921,0.0001};
    //f.search(v3); //vrijeme izvrsavanja 0ms
    //f.search2(v4); //vrijeme izvrsavanja 205ms
    //f.search3(v4); //vrijeme izvrsavanja 202ms

    std::cout<<std::endl;
    auto stop1 = std::chrono::high_resolution_clock::now();
    auto duration1 =std::chrono::duration_cast<std::chrono::milliseconds>(stop1 -start1);
    std::cout << "Vrijeme: " <<duration1.count() << "ms" << std::endl;

    return 0;
}


